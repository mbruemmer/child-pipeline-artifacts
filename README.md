# Parent-child pipeline artifacts handling demo

This pipeline demonstrates how to make parent artifacts available in child pipelines and vice versa. GitLab is tracking [an Epic](https://gitlab.com/groups/gitlab-org/-/epics/4019) for better artifact handling in parent-child pipelines.

The pipeline:

1. Creates a "dynamic" child pipeline in the `build` job. This is mocked by copying the [child pipeline file](child_pipeline_definition.yml) into the artifacts folder.
2. Triggers the child pipeline.
3. Pulls the parent pipeline artifacts into the child pipeline via a `.pre` stage job defined in the [pull_artifacts.yml](pull_artifacts.yml) and exposes them to the child pipeline. This is done via `needs` as described [here](https://docs.gitlab.com/ee/ci/yaml/#artifact-downloads-between-pipelines-in-the-same-project) 
4. Adds more files to the artifacts in the child pipeline.
5. Pulls the child pipeline artifacts back into the parent pipeline via the [artifacts API](https://docs.gitlab.com/ee/api/job_artifacts.html#download-the-artifacts-archive).
